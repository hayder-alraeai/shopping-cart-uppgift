package com.hayder.shoppingcart;

import com.hayder.shoppingcart.dto.OrderRequestDto;
import com.hayder.shoppingcart.models.ShoppingCartResponse;
import com.hayder.shoppingcart.models.ShoppingCartRow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext
public class ShoppingCartControllerTest {
    @LocalServerPort
    int port;
    @Autowired
    TestRestTemplate testRestTemplate;
    URI uri;

    @BeforeEach
    void setUp() throws URISyntaxException {
       uri  = new URI("http://localhost:" + port + "/shoppingcart");
    }

    @Test
    void create_shop_success_test() {
        ShoppingCartRow shoppingCartRow =
                new ShoppingCartRow("abc345", 99.99, 3.0);
        ShoppingCartRow shoppingCartRow2 =
                new ShoppingCartRow("abc345", 99.99, 2.0);

        OrderRequestDto orderRequestDto =
                new OrderRequestDto("abc123",
                        "order",
                        asList(shoppingCartRow, shoppingCartRow2));
        ResponseEntity<ShoppingCartResponse> res = testRestTemplate.postForEntity(uri, orderRequestDto, ShoppingCartResponse.class);
        System.out.println(res.getBody());
        assertThat(res.getStatusCode().value(), equalTo(200));
        assertThat(res.getBody().getCustomerNumber(), equalTo(orderRequestDto.getCustomerNumber()));
    }

    @Test
    void create_shop_fail_test() {
        OrderRequestDto orderRequestDto = new OrderRequestDto(null, null, List.of());
        ResponseEntity<ShoppingCartResponse> res = testRestTemplate.postForEntity(uri, orderRequestDto, ShoppingCartResponse.class);
        assertThat(400, is(res.getStatusCode().value()));
    }
}
