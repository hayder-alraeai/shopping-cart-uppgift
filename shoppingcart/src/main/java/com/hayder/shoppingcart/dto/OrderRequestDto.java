package com.hayder.shoppingcart.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hayder.shoppingcart.models.ShoppingCartRow;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.lang.NonNull;

import java.util.List;

@AllArgsConstructor
@Value
public class OrderRequestDto {
    @JsonProperty
    private String customerNumber;
    @JsonProperty
    private String orderNumber;
    @JsonProperty
    private List<ShoppingCartRow> shoppingCartRows;
}
