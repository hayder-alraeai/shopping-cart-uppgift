package com.hayder.shoppingcart.service;

import com.hayder.shoppingcart.models.ShoppingCartResponse;
import com.hayder.shoppingcart.models.ShoppingCartRow;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ShoppingCartService {

    private ShippingService shippingService;
    private PurchaseService purchaseService;
    public Optional<ShoppingCartResponse> placeOrder(String customerNumber,
                                                     String orderNumber,
                                                     List<ShoppingCartRow> shoppinCartRows) {
        if(customerNumber == null || orderNumber == null || shoppinCartRows.isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "VALUES CANNOT BE NULL");

                return Optional.of(new ShoppingCartResponse(customerNumber,
                        orderNumber,
                        shippingService.shipItem(shoppinCartRows)
                                .orElseGet(() -> {
                                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ERROR WHEN SHIPPING ITEMS");
                                }),
                        purchaseService.purchase(shoppinCartRows)
                                .orElseGet(() -> {
                                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ERROR WHEN PURCHASE");
                                }),
                        "SOME MESSAGE"
                        ));
    }
}
