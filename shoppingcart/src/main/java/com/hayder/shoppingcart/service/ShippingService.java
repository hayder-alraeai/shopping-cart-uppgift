package com.hayder.shoppingcart.service;

import com.hayder.shoppingcart.models.ShippingResponse;
import com.hayder.shoppingcart.models.ShoppingCartRow;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ShippingService {

    public Optional<ShippingResponse> shipItem(List<ShoppingCartRow> shoppingCartRows){
        return Optional.of(new ShippingResponse(shoppingCartRows,
                                                "Items above have been delivered"));
    }
}
