package com.hayder.shoppingcart.service;

import com.hayder.shoppingcart.models.PurchaseRequest;
import com.hayder.shoppingcart.models.PurchaseResponse;
import com.hayder.shoppingcart.models.ShoppingCartRow;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PurchaseService {
    RestTemplate restTemplate;

    public Optional<PurchaseResponse> purchase(List<ShoppingCartRow> shoppingCartRows){
        double totalPrice = shoppingCartRows.stream()
                .mapToDouble(s -> (s.getPrice() * s.getQuantity()))
                .sum();

        if (externalService(totalPrice, "some person", 1234) == null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "SOMETHING WENT WRONG WHEN PURCHASING");

        return Optional.of(externalService(totalPrice, "Hayder Alraeai", 12345678));
    }
    private PurchaseResponse externalService(double price, String cartHolder, long cartNumber){
        ResponseEntity<PurchaseResponse> res = restTemplate
                .postForEntity("http://localhost:8082/visa",
                        new PurchaseRequest(price, cartHolder, cartNumber), PurchaseResponse.class);
        return res.getBody();
    }
}
