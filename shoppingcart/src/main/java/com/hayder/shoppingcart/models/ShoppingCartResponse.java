package com.hayder.shoppingcart.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class ShoppingCartResponse {
    private String customerNumber;
    private String orderNumber;
    private ShippingResponse shippingResponse;
    private PurchaseResponse purchaseResponse;
    private String message;
}
