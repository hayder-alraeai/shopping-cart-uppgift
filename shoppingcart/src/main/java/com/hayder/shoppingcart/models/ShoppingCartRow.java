package com.hayder.shoppingcart.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class ShoppingCartRow {
    @JsonProperty
    private String itemNumber;
    @JsonProperty
    private double price;
    @JsonProperty
    private double quantity;
}
