package com.hayder.shoppingcart.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class ShippingResponse {
    private List<ShoppingCartRow> shoppingCartRows;
    private String message;

}
