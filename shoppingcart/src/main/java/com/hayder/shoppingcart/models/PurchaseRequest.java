package com.hayder.shoppingcart.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class PurchaseRequest {
    private double price;
    private String cartHolder;
    private long cartNumber;
}
