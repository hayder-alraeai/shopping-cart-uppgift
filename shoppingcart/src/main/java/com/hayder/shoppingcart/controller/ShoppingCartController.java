package com.hayder.shoppingcart.controller;

import com.hayder.shoppingcart.dto.OrderRequestDto;
import com.hayder.shoppingcart.models.ShoppingCartResponse;
import com.hayder.shoppingcart.service.ShoppingCartService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/shoppingcart")
@AllArgsConstructor
public class ShoppingCartController {
    private final ShoppingCartService shoppingCartService;

    @PostMapping
    public ShoppingCartResponse makeShop(@RequestBody OrderRequestDto orderRequestDto){
        return shoppingCartService
                .placeOrder(orderRequestDto.getCustomerNumber(),
                        orderRequestDto.getOrderNumber(),
                        orderRequestDto.getShoppingCartRows())
                .orElseGet(() -> {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "SOMETHING WENT WRONG!");
                });
    }
}
