package com.hayder.visapayment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VisaPaymentApplication {
    public static void main(String[] args) {
        SpringApplication.run(VisaPaymentApplication.class);
    }
}
