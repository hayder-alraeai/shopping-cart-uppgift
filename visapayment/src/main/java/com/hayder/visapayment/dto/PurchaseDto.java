package com.hayder.visapayment.dto;

import lombok.AllArgsConstructor;
import lombok.Value;

@AllArgsConstructor
@Value
public class PurchaseDto {
    private double price;
    private String cartHolder;
    private long cartNumber;
}
