package com.hayder.visapayment.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class PurchaseResponse {
    private double totalPrice;
    private String message;
}
