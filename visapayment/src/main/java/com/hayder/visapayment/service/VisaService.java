package com.hayder.visapayment.service;

import com.hayder.visapayment.dto.PurchaseDto;
import com.hayder.visapayment.models.PurchaseResponse;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class VisaService {
    public Optional<PurchaseResponse> makePayment(PurchaseDto purchaseDto){
        if (purchaseDto.getCartHolder() == null || purchaseDto.getCartNumber() == 0 || purchaseDto.getPrice() == 0)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "CART INFO CANNOT BE EMPTY");
        return Optional.of(new PurchaseResponse(purchaseDto.getPrice(), "Payment is succeed"));
    }
}
