package com.hayder.visapayment.controller;

import com.hayder.visapayment.dto.PurchaseDto;
import lombok.AllArgsConstructor;
import com.hayder.visapayment.models.PurchaseResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hayder.visapayment.service.VisaService;
import org.springframework.web.server.ResponseStatusException;

@AllArgsConstructor
@RestController
@RequestMapping("/visa")
public class VisaController {
    private VisaService visaService;
    @PostMapping
    public PurchaseResponse getPayment(@RequestBody PurchaseDto purchaseDto){
        return visaService.makePayment(purchaseDto)
                .orElseGet(() -> {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "SOMETHING WENT WRONG IN PURCHASE RESPONSE");
                });
    }
}
