package com.hayder.visapayment;

import com.hayder.visapayment.dto.PurchaseDto;
import com.hayder.visapayment.models.PurchaseResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.net.URISyntaxException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class VisaControllerTest {
    @LocalServerPort
    int port;
    @Autowired
    TestRestTemplate testRestTemplate;
    URI uri;

    @BeforeEach
    void setUp() throws URISyntaxException {
        uri = new URI("http://localhost:" + port + "/visa");
    }

    @Test
    void purchase_success() {
        ResponseEntity<PurchaseResponse> res = testRestTemplate
                .postForEntity(uri,
                        new PurchaseDto(10.0, "hayder", 123), PurchaseResponse.class);
        assertThat(200, equalTo(res.getStatusCode().value()));
    }

    @Test
    void purchase_fail() {
        ResponseEntity<PurchaseResponse> res = testRestTemplate
                .postForEntity(uri,
                        new PurchaseDto(0.0, null, 0), PurchaseResponse.class);
        assertThat(400, equalTo(res.getStatusCode().value()));
    }
}
